<?php 
    session_start();
    if(!isset($_SESSION['nomina']))
        header("Location: login.php");
    
    $user = $_SESSION['user'];
    include 'header.php';
?>
<section class='container'>
    <div class="row justify-content-md-center">
        <div class='col-8 text-center'>
            <h1>BIENVENID@ <?php echo $user->nombre ?></h1>
        </div>
    </div>
    <div class="row justify-content-md-center">
        <table class='table'>
            <thead>
                <tr>
                    <th colspan='16'>SEMANAS</th>
                </tr>
                <tr>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th>11</th>
                    <th>12</th>
                    <th>13</th>
                    <th>14</th>
                    <th>15</th>
                    <th>16</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <?php
                        $fecha = $user->ingreso;
                        $datetime1 = new DateTime($fecha);//start time
                        $datetime2 = new DateTime(date("Y-m-d"));//end time
                        $interval = $datetime1->diff($datetime2);
                        $dias = intval($interval->format('%a'));
                        $semanas = intval($dias / 7);
                        $d1 = "";
                        $d2 = "";
                        $d3 = "";
                        $d4 = "";
                        $d5 = "";
                        $d6 = "";

                        for($i = 1; $i<=16;$i++){
                            if($i<=$semanas){
                                echo "<td class='table-success'>OK</td>";
                            } else {
                                echo "<td class='table-danger'></td>";
                                if($i==1){
                                    $d1 = "disabled";
                                }
                                if($i==2){
                                    $d2 = "disabled";
                                }
                                if($i==3){
                                    $d3 = "disabled";
                                }
                                if($i==4){
                                    $d4 = "disabled";
                                }
                                if($i==8){
                                    $d5 = "disabled";
                                }
                                if($i==12){
                                    $d6 = "disabled";
                                }
                            }
                        }
                    ?>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="row justify-content-md-center">
        <div class='col text-center'><a href='cuestionario.php?id=1'><button class='btn btn-primary' <?php echo $d1; ?>>Primer Semana</button></a></div>
        <div class='col text-center'><a href='cuestionario.php?id=2'><button class='btn btn-primary' <?php echo $d2; ?>>Segunda Semana</button></a></div>
        <div class='col text-center'><a href='cuestionario.php?id=3'><button class='btn btn-primary' <?php echo $d3; ?>>Tercer Semana</button></a></div>
    </div>
    <br />
    <div class="row justify-content-md-center">
        <div class='col text-center'><a href='cuestionario.php?id=4'><button class='btn btn-primary' <?php echo $d4; ?>>Cuarta Semana</button></a></div>
        <div class='col text-center'><a href='cuestionario.php?id=5'><button class='btn btn-primary' <?php echo $d5; ?>>Segunda Mes</button></a></div>
        <div class='col text-center'><a href='cuestionario.php?id=6'><button class='btn btn-primary' <?php echo $d6; ?>>Tercer Mes</button></a></div>
    </div>
</section>
<?php
    include 'footer.php';
?>